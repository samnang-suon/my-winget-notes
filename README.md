# My Winget Notes
## Official Doc
https://docs.microsoft.com/en-us/windows/package-manager/winget/
## List of CLI options
![winget-help](images/winget-help.png)
## The Most Useful Commands
|Command|Description|
|-------|-----------|
winget COMMAND_NAME -? | show COMMAND_NAME help
winget list | List installed packages
winget upgrade | List packages which have a newer version available
winget upgrade --all --force | Upgrade all packages to their latest available versions
winget install --exact --id PACKAGE_ID | Install PACKAGE_ID package
winget uninstall --name PACKAGE_NAME | Uninstall PACKAGE_NAME package
winget uninstall --id PACKAGE_ID | Uninstall PACKAGE_ID package
## Winget Packages Repo
https://winget.run/
### Some Package Examples
* winget install --exact --id GoLang.Go
* winget install --exact --id Microsoft.VisualStudioCode
